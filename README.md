## CSS 1
### 1 Getting Started
    * There are 3 ways to use css:
        - A separate css file:
            + Always referenced on <head> tag
            + Use <link> with 2 attribute "rel" & "href"
        - Inline css with "style" attribute
        - Internal css in <style> tag
    * Naming convention:
        - Use lowercase letters    
        - Do not use space or special symbols
        - Use - or _ to seperate words
        - Meaningful name
### 2 CSS Core
    * Use to separate presetation from content
    * Syntax:
        - Selectors determine which html element to apply styles to
        - Wrapped by declaration block {}
        - Declaration written by: property:value; pair
    * 3 ways to apply css:
        - Type selector: html element type will be applied
        - Class selector: 
            + Can be use multiple times in same page, style will apply to any element with class attribute.
            + Class selector start with .
        - Id selector: 
            + Can only be use once per page, unique value.    
            + Id selector start with #
    * Descendant selector:
        - Use multiple selectors, separate by a space. ex: body p { }  
    * Grouping multiple selector:
        - Use multiple selector, separate by ,
    * Pseudo-class selector:
        - Pseudo-class specify a state of element
        - Syntax: selector:state {}
    * Comment syntax: a /* */ pair
    * CSS color:
        - Use real name
        - Use 6 hexacode after start with #
        - RGB scale, each between 0 and 255. Ex: rgb(255,255,255)
    * Some element can inherit their parent style
    * CSS Specify:
        - The ranking of CSS specify: Type < Class < id selector
        - The style of the higher rank will overwrite the style of the lower rank regardless order.
        
### 3 Typography
    * Typography: the study of the design and use of type for communication
    * Typeface: set of fonts
    * Web-safe fonts: font that commonly preinstalled on computers or devices. Ex: Arial, Times New Roman
    * Internal fonts: downloaded font files, included in project like css and js files.
    * Font-faces:
        -  Use to set the font name and link to the font files
        -  Syntax: 
            @ font-face {
                font-family: 'font_name';
                src: url/parenthese/exact_name
            }
    * External fonts:
        - Third-party online services
        - No need to download
        - Linked directly to CSS and font files hosted online       
    * Font size property:
        - px: 
            + Common and straightforward
            + Absolute value
            + Use whole number and avoid decimals
        - em:
            + Relative unit
            + Same as closest parent font size
            + If no font-size is declare, 1 em = 16px in default
            + Use decimals number
        - rem    
            + Similar with em
            + Relative only html style
### 4 Layouts
    * Block & Inline
        - Block element
            + Height = content
            + Width = 100% container
            + Can wrap other block element or inline element
        - Inline
            + Height = width = content
        - Anchor element <a> can wrap up block element
    * Box model
        - Width & height: set specific size of the box
        - Padding: space inside of element
        - Margin: space outside
        - Border: space between padding & margin
    * Float
        - Can be use to rearrange how elements will be displayed   
        - Clear proprerty can be use to clear float
        - If element's content higher than its height, overflow property can be used
    * Box-sizing fix:
        - via: https://www.paulirish.com/2012/box-sizing-border-box-ftw/
        html {
            box-sizing: border-box;
        }

        *, *:before, *:after {
            box-sizing: inherit;
        }         


## CSS 2
### 1 CSS Selectors        
    * Attribute selector
        - Matched attribute: 
            [attribute] {

            }
        - Matched attribute & value:
            [attribute=value] {

            }
    * Combine selector
        - Descendant selector:
            + body p {

            }
        - Child selector: 
            + body > p {

            }   
        - Sibling selector
            + Adjancent: apply style to element2 that right after the element1
              h1 + p {

              }   
            + General: apply to element2 that appearn after the element1
              h1 ~ p {

              }       
    * Pseudo-class
        - First child: :first-child
        - Last child: :last-child
        - First of type: :first-of-type
        - Last of type:  :last-of-type
        - N child: :nth-child(): can be keyword, number, expression
            + Key word: odd & even
            + Number: exact which child
    * Pseudo-element:
        - Before & after:
            + Always be used with "content" property
            + Use string value, enclose in quote           
### 2 Layouts
    * Horizontal navs:
        - Using float
        - Using inline-block style
    * Positioning: 
        - Used to arrange elements relative to the default page flow or browser viewport
        - 5 values: 
            + relative, absolute, fixed: can be positioned within a containing element, browser viewport
            + static: element not positioned
            + inherit: inherit values from ancestor
        - Positioning can also used with offset value (top, right, bottom, left)             
    * Layer & z-index
        - z-axis: element with higher value stand on top of the other
        - Natural stacking order: block < float < inline < position      
        - z-index value can be customize to specific value.
### 3 Tips & Tools
    * Reset stylesheet: prewritten stylesheet containing rules turn browser style to un-styled baseline
    * Nomarlize: prewritten stylesheet aim to create consistent default style
    * Icon font:
        - Add image without using image
        - Customize by CSS
        - Ex: Font-Awesome, glyphicon
### 4 Responsive & Mobile
    * Some kind of layouts:
        - Static
        - Adaptive
        - Liquid
        - Responsive
    * Graceful degradation:
        - Design for modern browser first
        - Provide fallback for unsupported features
        - Ensure basic functionality work with older browser
    * Progressive enhancement:
        - Focus on content and accessibility first
        - Create base-level experience first
        - Add advance features for enhancement
    * Creating flexible and fluid layouts
        - Fluid layout are responsive, but the content and component only get narrower or wider
        - Use percentage-based width for page components
        - Use min- or max-widths to add constraint to the layout
        - Can use those 2 together      
    * Media queries:
        - Are added to CSS using @media{} keyword
        - Try not to use many media queries          